---
aliases:
  - /akregator
layout: component
title: Akregator
screenshot: /assets/img/kontact-akregator.png
shortDescription: Stay on top of the news with our RSS reader.
description: >
    Akregator is an RSS feed reader. It can automatically download RSS feeds
    from your favorite sites and allows you to read the articles even when you
    are offline. Also provides rich search functionality and archiving features.
weight: 40
---

Akregator helps you to keep informed about new stories on websites like KDE
Dot News and Planet KDE blogs. The technology used is RSS and many sites
support it.

## Features
* Simple to use
* Can accept dozens of feeds
* Can notify you of unread feeds
* Uses tabs to give access to internal reading of full stories
* Feeds archive
* Import and export feeds

