#!/usr/bin/env bash

# failures are evil
set -e

# fetch the dependencies
# hugo mod get

# run hugo in local server mode, show all stuff
exec hugo server -D --buildFuture
